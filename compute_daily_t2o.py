import os
import json
import glob, yaml
import sys
import numpy as np
import pandas as pd
import requests
from pymongo import MongoClient
from datetime import datetime, timedelta

names = ['order_time', 'order_ts', 'cancel_ts', 'terminate_ts', 'latency', 'send_cost', 'cid','sid','account', 'symbol',
        'order_price', 'received_order_price', 'order_size', 'direction', 'offset', 'type',
        'status', 'ec', 'vol', 'pnl', 'vwap', 'fee','taker_vol', 'maker_vol','taker_vwap',
        'signal_nt', 'signal_ts', 'data_ts', 'exchange_ts', 'signal', 'riskL', 'riskS', 'riskLimit',
        'bid0','bidsize0','ask0','asksize0', 'bid1', 'bidsize1', 'ask1', 'asksize1', 'maker_time', 'processed_amount']
old_names = ['order_time', 'order_ts', 'cancel_ts', 'terminate_ts', 'latency', 'send_cost', 'cid','sid','account', 'symbol',
        'order_price', 'received_order_price ', 'order_size', 'direction', 'offset', 'type',
        'status', 'vol', 'pnl', 'vwap', 'fee','taker_vol', 'maker_vol','taker_vwap',
        'signal_nt', 'signal_ts', 'data_ts', 'exchange_ts', 'signal', 'riskL', 'riskS', 'riskLimit',
        'bid0','bidsize0','ask0','asksize0', 'bid1', 'bidsize1', 'ask1', 'asksize1', 'maker_time', 'processed_amount']

usecols=['order_time', 'symbol', 'latency', 'order_ts', 'signal_ts', 'signal_nt', 'data_ts', 'exchange_ts', 'terminate_ts', 'cancel_ts', 'account', 'type', 'direction', 'asksize0', 'bidsize0', 'order_size', 'vol']

last_update_address = '/home/souyang/last_updated_post_trades.yaml'
if os.path.exists(last_update_address):
    with open(last_update_address, 'r') as f:
        last_updates = yaml.load(f, Loader=yaml.FullLoader)
else:
    last_updates = {}

def stats(x):
    d = {}
    takers = x[x.type=='T']
    takers.loc[takers.direction=='B', 'fill_ratio'] = (takers.loc[takers.direction=='B', 'vol'] / takers.loc[takers.direction=='B',['asksize0', 'order_size']].min(axis=1).replace(0,1))
    takers.loc[takers.direction=='S', 'fill_ratio'] = (takers.loc[takers.direction=='S', 'vol'] / takers.loc[takers.direction=='S',['bidsize0', 'order_size']].min(axis=1).replace(0,1))
    cancel_orders = x[((x.order_size - x.vol).abs() > 1e-4) & (~x.cancel_ts.isnull())].copy()
    if cancel_orders.shape[0] > 10:
        d.update( (cancel_orders['terminate_ts'] - cancel_orders['cancel_ts']).describe(percentiles=[0.95,0.99]).fillna(-1).astype(int)[['99%', '95%']].rename(index={'99%': 'cancel_cost_99', '95%': 'cancel_cost_95'}).to_dict() )
    d.update( (takers['fill_ratio'].clip(upper=1).describe(percentiles=[0.1,0.5]).fillna(-1)[['mean']].rename(index={'mean': 'fillratio_avg'})*100).astype(int).to_dict() )
    d.update( (x['signal_nt']- x['signal_ts']).describe(percentiles=[0.95,0.99]).fillna(-1).astype(int)[['99%','95%']].rename(index={'99%': 'signal_cost_99', '95%': 'signal_cost_95'}).to_dict() )
    d.update( (x['order_ts']- x['signal_nt']).describe(percentiles=[0.95,0.99]).fillna(-1).astype(int)[['99%','95%']].rename(index={'99%': 'order_cost_99', '95%': 'order_cost_95'}).to_dict() )
    d.update( (x['data_ts']- x['exchange_ts']).describe(percentiles=[0.95,0.99]).fillna(-1).astype(int)[['99%','95%']].rename(index={'99%': 'ets_latency_99', '95%': 'ets_latency_95'}).to_dict() )
    d.update( (x['latency']).describe(percentiles=[0.95,0.99]).fillna(-1).astype(int)[['99%','95%']].rename(index={'99%': 'ote_latency_99', '95%': 'ote_latency_95'}).to_dict() )
    
    if 'cancel_cost_99' in d:
        return pd.Series(d, index= ['signal_cost_99', 'signal_cost_95', 'order_cost_99', 'order_cost_95', 'ets_latency_99', 'ets_latency_95', 'ote_latency_99', 'ote_latency_95', 'cancel_cost_99', 'cancel_cost_95','fillratio_avg'])
    else:
        d.update({'cancel_cost_99': '', 'cancel_cost_95': ''})
        return pd.Series(d, index= ['signal_cost_99', 'signal_cost_95', 'order_cost_99', 'order_cost_95', 'ets_latency_99', 'ets_latency_95', 'ote_latency_99', 'ote_latency_95', 'cancel_cost_99', 'cancel_cost_95','fillratio_avg'])

def hb_stats(x):
    d = {}
    takers = x[x.type=='T']
    takers.loc[takers.direction=='B', 'fill_ratio'] = (takers.loc[takers.direction=='B', 'vol'] / takers.loc[takers.direction=='B',['asksize0', 'order_size']].min(axis=1).replace(0,1))
    takers.loc[takers.direction=='S', 'fill_ratio'] = (takers.loc[takers.direction=='S', 'vol'] / takers.loc[takers.direction=='S',['bidsize0', 'order_size']].min(axis=1).replace(0,1))
    cancel_orders = x[((x.order_size - x.vol).abs() > 1e-4) & (~x.cancel_ts.isnull())].copy()
    if cancel_orders.shape[0] > 10:
        d.update( (cancel_orders['terminate_ts'] - cancel_orders['cancel_ts']).describe(percentiles=[0.95,0.99]).fillna(-1).astype(int)[['99%', '95%']].rename(index={'99%': 'cancel_cost_99', '95%': 'cancel_cost_95'}).to_dict() )
    d.update( (takers['fill_ratio'].clip(upper=1).describe(percentiles=[0.1,0.5]).fillna(-1)[['mean']].rename(index={'mean': 'fillratio_avg'})*100).astype(int).to_dict() )
    d.update( (x['signal_nt']- x['signal_ts']).describe(percentiles=[0.95,0.99]).fillna(-1).astype(int)[['99%','95%']].rename(index={'99%': 'signal_cost_99', '95%': 'signal_cost_95'}).to_dict() )
    d.update( (x['order_ts']- x['signal_nt']).describe(percentiles=[0.95,0.99]).fillna(-1).astype(int)[['99%','95%']].rename(index={'99%': 'order_cost_99', '95%': 'order_cost_95'}).to_dict() )
    d.update( (x['latency']).describe(percentiles=[0.95,0.99]).fillna(-1).astype(int)[['99%','95%']].rename(index={'99%': 'ote_latency_99', '95%': 'ote_latency_95'}).to_dict() )

    if 'cancel_cost_99' in d:
        return pd.Series(d, index= ['signal_cost_99', 'signal_cost_95', 'order_cost_99', 'order_cost_95', 'ote_latency_99', 'ote_latency_95', 'cancel_cost_99', 'cancel_cost_95', 'fillratio_avg'])
    else:
        d.update({'cancel_cost_99': '', 'cancel_cost_95': ''})
        return pd.Series(d, index= ['signal_cost_99', 'signal_cost_95', 'order_cost_99', 'order_cost_95', 'ote_latency_99', 'ote_latency_95', 'cancel_cost_99', 'cancel_cost_95', 'fillratio_avg'])


def get_stats(argv):
    server = argv[0]
    date = argv[1]
    date_end = argv[2]
    if len(argv) > 3:
        last_updates = argv[3]

    addresses = glob.glob("/shared/crypto/trades/{}/trade_{}*".format(server,date))
    last_date = (datetime.strptime(date, '%Y-%m-%d') - timedelta(days=1)).strftime('%Y-%m-%d')
    last_path = "/shared/crypto/trades/{}/trade_{}_23-59.txt".format(server,last_date)
    if os.path.exists(last_path):
        addresses.append(last_path)
    next_path = "/shared/crypto/trades/{}/trade_{}_23-59.txt".format(server,date)
    if os.path.exists(next_path):
    	addresses.remove(next_path)
    addresses = sorted(addresses)
    if not addresses:
        print('no matching addresses selected: ', server)
        return [server]
    
    last_update_addresses = '' if server not in last_updates else last_updates[server]
    if addresses[-1] == last_update_addresses:
        print('addresses not updated')
        return [server]
    last_updates[server] = addresses[-1]
    dfs = []
    for p in sorted(addresses):
        tmp = pd.read_csv(p, names=old_names, low_memory=False).loc[:, usecols]
        if tmp.empty:
            continue
        try:
            #(tmp.index[0] == 0 and tmp.terminate_ts.iloc[0] > 1e8)
            if tmp.index[0] == 0 and ('signal' not in tmp.columns or tmp.signal.astype(str).str.contains('|').sum() > 10):
                tmp = tmp.iloc[5:]
                dfs.append(tmp)
            else:
                tmp = pd.read_csv(p, names=names, usecols=usecols, low_memory=False)
                tmp = tmp.iloc[5:]
                dfs.append(tmp)
        except Exception as e:
            print(str(e))
            print(p)
            print(tmp.index[0], tmp.terminate_ts.iloc[0], tmp.iloc[0])
            tmp = pd.read_csv(p, names=names, usecols=usecols, low_memory=False)
            dfs.append(tmp)
        #if tmp.empty:
        #   continue
        #tmp = tmp.iloc[5:]
        #dfs.append(tmp)
            
    df = pd.concat(dfs, sort=False)
    # if df.symbol.value_counts().shape[0] > 5:
    #     df = df[df.symbol.isin(df.symbol.value_counts().index[:-3])].reset_index(drop=True)

    print('total_trades: ', df.shape[0], ' server: ', server, ' with fields: ', df.columns)
    df = df[~df.isnull().any(axis=1)].sort_values(by='order_time').copy()
    df.loc[:, 'order_time'] = pd.to_datetime(df.order_time)
    #df.loc[:, 'ets'] = df.data_ts-df.exchange_ts

    time_aligned = pd.DataFrame(pd.date_range(start=date, end=date_end, freq='15min'), columns=['order_time'])
    time_aligned.loc[:, 'time'] = time_aligned.order_time

    merged = pd.merge_asof(df, time_aligned, on='order_time')
    if merged.shape[0] < 10:
       print('not enough qualified trades: ', server)
       return [server]
    
    if 'HB' in merged.account.iloc[0]:
        #merged.loc[:, 'type'] = merged.symbol.apply(lambda x: 'tf0' if x.endswith('tf0') else 'nf')
        sym_res = merged.groupby(['symbol', 'time']).apply(hb_stats).reset_index()
        all_res = merged.groupby('time').apply(hb_stats).reset_index()
    else:
        #merged.loc[:, 'type'] = merged.symbol.apply(lambda x: 'f' if x.endswith('f0') else 'nf')
        sym_res = merged.groupby(['symbol', 'time']).apply(stats).reset_index()
        all_res = merged.groupby('time').apply(stats).reset_index()
    all_res.loc[:, 'symbol'] = 'ALL'
    #final_res = all_res.copy()
    final_res = pd.concat([sym_res, all_res], axis=0).reset_index(drop=True)
    final_res.loc[:, 'time'] = final_res['time'].astype(str)
    #print(final_res.time.sort_values(), server)
    return final_res.to_dict(orient='records'), server


def send_email(user, pwd, recipient, subject, body):
    import os
    import sys
    import smtplib
    from configparser import ConfigParser
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

    _send_server = "smtp.exmail.qq.com"
    _send_port = "465"
    _send_with_ssl = True

    _from = user
    _to = recipient if type(recipient) is list else [recipient]

    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = _from
    msg['To'] = ','.join(_to)
    html = "<html><head></head><body>"
    html += body.replace('\n', ' <br>').replace(' ', '&nbsp;')
    html += "</body></html>"
    msg.attach(MIMEText(html, 'html', 'utf-8'))

    try:
        server = smtplib.SMTP_SSL(_send_server, port=_send_port) \
            if _send_with_ssl else smtplib.SMTP(_send_server, port=_send_port)
        server.login(user, pwd)
        server.sendmail(_from, _to, msg.as_string())
        server.close()
        print('Successfully sent the mail')
    except:
        import traceback
        traceback.print_exc(file=sys.stdout)
        print('Failed to send mail')

if __name__ == "__main__":
    
    last_update_address = '/home/souyang/crypto_trades_t2o/last_updated_post_trades.yaml'
    if os.path.exists(last_update_address):
        with open(last_update_address, 'r') as f:
            last_updates = yaml.load(f, Loader=yaml.FullLoader)
    else:
        last_updates = {}
    
    if len(sys.argv) > 1:
        prev_d = int(sys.argv[1])
    else:
        prev_d = 0
    date = ( datetime.utcnow() - timedelta(days=prev_d)).strftime('%Y-%m-%d')
    date_end = (datetime.now() - timedelta(days=prev_d) + timedelta(days=1)).strftime('%Y-%m-%d')
    options = [ ['tkc23', date, date_end, last_updates], ['tka1', date, date_end, last_updates], ['tka2', date, date_end, last_updates],
                ['tkc2', date, date_end, last_updates], ['tkc3', date, date_end, last_updates], ['tka3', date, date_end, last_updates],
                #['hkc2', date, date_end, last_updates], ['hkc3', date, date_end, last_updates],
                ['tkc25', date, date_end, last_updates], ['tkc12', date, date_end, last_updates],
                ['sgc1', date, date_end, last_updates], ['sgc2', date, date_end, last_updates], ['tkd1', date, date_end, last_updates]
                ]
            
    print(date, date_end)
    url = 'http://47.57.5.75/reboam/api/latency'
    #url = 'http://192.168.8.249:3000/api/latency'
    from multiprocessing import Pool
    pool = Pool(10)
    
    recs = pool.map(get_stats, options)
    try:
        for rec in recs:
            if len(rec) != 2:
                print(rec, ' skipped')
                continue
            df, arg1 = rec[0], rec[1]
            print(arg1, df)
            print(arg1, ' completed')
            if arg1 == 'tkc23' or arg1 == 'tkc24' or arg1 == 'tka1' or arg1== 'tka2':
                exchange = 'BNBFUT'
            elif arg1 == 'tkc25':
                exchange = 'BNBSPOT'
            elif arg1.startswith('hkc'):
                exchange = 'OKCFUT'
            elif arg1 == 'tkc12':
                exchange = 'HBSPOT'
            elif arg1.startswith('tkc') and len(arg1)==4:
                exchange = 'HBFUT'
            elif arg1.startswith('sgc'):
                exchange = 'BYBFUT'
            elif arg1.startswith('tkd'):
                exchange = 'FTXFUT'
            elif arg1 == 'tka3':
                exchange = 'GTFUT'

            if df:
                requests.post(url=url, data=json.dumps({'server': arg1, 'exchange': exchange, 'data': df}), timeout=15, headers={'Content-Type': 'application/json'}) 
    except Exception as error:
        print(str(error))
        #send_email('report@berkeleybrothers.com', '2201Full',
                #'samuel.ouyang@berkeleybrothers.com',
                #'Crypto Trades T2O ERROR: ' , str(error))
    with open(last_update_address, 'w') as f:
        d = yaml.dump(last_updates, f)

